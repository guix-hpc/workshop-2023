#+TITLE: How to get started using Guix
#+AUTHOR: Ludovic Courtès
#+DATE: November 2023

This repository contains material for the tutorials “How to get started…”
at the [[https://hpc.guix.info/events/2023/workshop/][Workshop on
Reproducible Software Environments for Research and High-Performance
Computing]] held in Montpellier, France, November 8–10, 2023.

The two tutorials [[./getting-started/hands-on.org][“How to get started using Guix”]] and [[./packaging/][“How to get
started writing Guix packages”]] can be found in their respective
subdirectories.

Published under [[https://creativecommons.org/licenses/by-sa/4.0/deed.fr][CC-BY-SA 4.0]].
