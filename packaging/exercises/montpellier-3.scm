(define-module (guix-ui)
  #:use-module (guix)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages))

(define-public st-puzzles
  (package
    (name "st-puzzles")
    (version "20231110")
    (synopsis "Simon Tatham's portable puzzle collection")
    (description "This package contains a collection of small computer
programs which implement one-player puzzle games.")
    (home-page "https://www.chiark.greenend.org.uk/~sgtatham/puzzles/")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://git.tartarus.org/simon/puzzles.git")
              (commit "35f796542ee5d7e775643f526973aa75f483bf20")))
        (file-name (git-file-name name version))
        (sha256 (base32 "1yawzlx247mkkccxcc55zdpqpnav0a8mjgirmfg6hviniyb6ahya"))))
    (build-system cmake-build-system)
    (arguments (list #:configure-flags #~(list "--enable-silent-rules")
                     #:build-type "Release"))
    (license license:expat)))
;; This allows you to run guix shell -f example.scm.
;; Remove this line if you just want to define a package.
st-puzzles
