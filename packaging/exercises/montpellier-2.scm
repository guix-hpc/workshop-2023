(define-module (montpellier-2)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (gnu packages python-xyz))

(define-public python-numpy-illustrated
  (package
    (name "python-numpy-illustrated")
    (version "0.3")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "numpy-illustrated" version))
       (sha256
        (base32 "1x3gd19hhmlg51i5aj070y7w0lk47gx6yx3r3f45396bgdfnsw4i"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-numpy))
    (home-page "https://github.com/axil/numpy-illustrated")
    (synopsis "Helper functions from the NumPy Illustrated guide")
    (description "This package provides helper functions for the
@url{https://betterprogramming.pub/numpy-illustrated-the-visual-guide-to-numpy-3b1d4976de1d?sk=57b908a77aa44075a49293fa1631dd9b,
NumPy Illustrated} programming guide.")
    (license license:expat)))
