\documentclass [xcolor=x11names,t]{beamer}

\usepackage {fontspec,xunicode,xltxtra}
\hypersetup {colorlinks=true,urlcolor=Purple1}

\usetheme {GuixAE}
\setsansfont {Libertinus Sans}
\AtBeginSection[]
{
  \begin{frame}<beamer>
  \frametitle{\inserttitle}
  \tableofcontents[currentsection]
  \end{frame}
  \addtocounter {framenumber}{-1}
}

\title [\textcolor {white}{Writing Guix packages}]
   {How to get started writing Guix packages}
\date [Workshop on Reproducible Software Environments for Research]{First
Workshop on Reproducible Software Environments \\
for Research and High-Performance Computing \\
Montpellier \\
10 November 2023}

\newcommand {\codeline}[1]{\begin {center}\tt #1 \end {center}}

\begin{document}

\begin {frame}
   \maketitle
   \addtocounter {framenumber}{-1}
\end {frame}


\section {Using an importer}

\begin {frame}{Importing packages from language repositories}
\begin {itemize}
\item
Prerequisites: none!
\item
Digression
\begin {itemize}
\item
Many Guix developers use Emacs.
\item
Many potential Guix contributors think that Emacs is a requirement.
\item
This is a myth. You can also use a text editor.
\item
Difficulty: Guix is so flexible that there is no canonical way.
\end {itemize}
\item
Available importers:
\codeline {guix import --help}

gnu, pypi, cpan, hackage, stackage, egg, elpa, gem, go,
cran, crate, texlive, json, opam, minetest, elm, hexpm
\end {itemize}
\end {frame}


\begin {frame}[fragile]{Import a PyPI package}
\begin {itemize}
\item
Find an interesting package.

\url {https://pypi.org/project/numpy-illustrated/}
\item
Import it

\codeline {guix import pypi numpy-illustrated}

This prints a record.
\item
Put it into a file.

\begin {verbatim}
mkdir proj
guix import pypi numpy-illustrated > proj/montpellier.scm
\end{verbatim}
\item
Compile it

\codeline {guix build -f proj/montpellier.scm}
\item
Boilerplate! Copy-paste it from elsewhere, or let the system help you.
\end {itemize}
\end{frame}


\begin {frame}[fragile]{Import a PyPI package}
\begin {itemize}
\item
While we are at it, turn the value into a public variable \\
and encapsulate it into its own module:

\begin {verbatim}
(define-module (montpellier)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (gnu packages python-xyz))
  
(define-public python-numpy-illustrated
  (package
    ...
    (license license:expat)))
\end{verbatim}
\item
Compile the package using its name
\codeline {guix build -L proj python-numpy-illustrated}
\end {itemize}
\end{frame}


\begin {frame}{Import a PyPI package}
\begin {itemize}
\item
Is it really there?
\codeline {guix package -L proj -A python-numpy}
\item
Check the package.
\codeline {guix lint -L proj python-numpy-illustrated}
\item
Improve the description.
\item
\href {solutions/montpellier-1.scm}{Solution}
\item
Useful tip: recursive importer
\codeline {guix import pypi -r scipy}
\end {itemize}
\end {frame}


\section {Updating a package}

\begin {frame}{Update a package}
\begin {itemize}
\item
Even easier! Download an
\href {exercises/montpellier-2.scm}{outdated package}.
\item
Check for updates.
\codeline {guix refresh -L \$PWD/proj python-numpy-illustrated}
(That we need an absolute path here seems to be a bug in Guix.)
\item
Update in place.
\codeline {guix refresh -L \$PWD/proj -u python-numpy-illustrated}
\item
Admire the change.
\codeline {diff exercises/montpellier-2.scm proj/montpellier-2.scm}
\item
Build the package.
\codeline {guix build -L proj python-numpy-illustrated}
\end {itemize}
\end {frame}


\begin {frame}[c]{Exercise}
\Huge
Import your favourite package.
\end {frame}


\section {Creating a package from scratch}

\begin {frame}{Start easily}
\begin {itemize}
\item
Find an interesting package. \\
Simon Tatham's Portable Puzzle Collection \\
\url {https://www.chiark.greenend.org.uk/~sgtatham/puzzles/}
\item
Check the license. \\
\url {https://www.chiark.greenend.org.uk/~sgtatham/puzzles/doc/licence.html\#licence}
\item
Find the source code. \\
\url {https://git.tartarus.org/simon/puzzles.git}
\item
Create a package skeleton. \\
\url {https://guix-hpc.gitlabpages.inria.fr/guix-packager/} \\
Thanks to Philippe Virouleau! \\
Download result to \texttt {proj/montpellier-3.scm}
\item
Try to build.
\codeline {guix build -L proj st-puzzles \textcolor {red}{-K}}
\end {itemize}
\end {frame}


\begin {frame}{Complete the package}
\begin {itemize}
\item
Add \texttt {native-inputs}. \\
Here: pkg-config
\item
Add \texttt {inputs}. \\
Here: gtk+
\item
Disable tests.
\item
\ldots
\item
\href {solutions/montpellier-3.scm}{Solution}
\item
Run the games!
\codeline {guix shell -L proj st-puzzles -- mines}
\end {itemize}
\end {frame}


\section {Channelling your energy}

\begin {frame}[fragile]{Create a custom channel}
\begin {itemize}
\item
Move your package to git.
\begin {verbatim}
cd proj
git init .
git add .
git commit
cd ..
\end{verbatim}
\item
Create a channel.
\begin {verbatim}
cat > channels.scm << EOF
(cons* (channel
         (name 'montpellier)
         (url "file://`pwd`/proj"))
       %default-channels)
EOF
\end{verbatim}
\item
Use the channel.
\begin {verbatim}
guix time-machine -C channels.scm -- \
   build python-numpy-illustrated
\end{verbatim}
\end {itemize}
\end{frame}

\begin {frame}{Make things permanent}
Several options.
\begin {enumerate}
\item
Work for yourself.
\begin {itemize}
\item
Move the \texttt {channels.scm} file to \\
\texttt {\$HOME/.config/guix/channels.scm}
\item
Update the Guix view of the world.
\codeline {guix pull}
\end {itemize}
\item
Work with a group.
\begin {itemize}
\item
Move the git repository to a shared server.
\item
Replace \\
\hspace {1cm} \texttt {file:///...} \\
by something like \\
\hspace {1cm} \texttt {https://gitlab.inria.fr/guix-hpc/guix-hpc.git}
\end {itemize}
\item
Work with the world.
\begin {itemize}
\item
Contribute your package to Guix upstream.
\end {itemize}
\end {enumerate}
\end {frame}


\begin {frame}[c]{Exercise}
\Huge
Create your favourite package.
\end {frame}


\section {Contributing to Guix}

\begin {frame}[fragile]{Set up Guix}
\begin {itemize}
\item
See here: \\
{\small \url {https://guix.gnu.org/manual/devel/en/guix.html\#Contributing}}
\item
No account is needed!
\item
Clone the repository.
\codeline {git clone https://git.savannah.gnu.org/git/guix.git}
\item
Put yourself in an environment with all Guix dependencies.
\codeline {guix shell -D guix --pure}
\item
Build Guix.
\begin {verbatim}
cd guix
./bootstrap
./configure --localstatedir=/var --sysconfdir=/etc
make -j 4
\end{verbatim}
{\ldots} and wait
\end {itemize}
\end{frame}


\begin {frame}[fragile]{Test the package in situ}
\begin {itemize}
\item
Create a branch.
\codeline {git checkout -b montpellier}
\item
Put the package into an existing module; say, close to
\texttt {python-numpy}.
\codeline {./pre-inst-env guix edit python-numpy}
\item
Build it.
\begin {verbatim}
make
./pre-inst-env guix build python-numpy-illustrated
\end{verbatim}
\item
Commit it.
\codeline {./pre-inst-env etc/committer.scm}
\end {itemize}
\end{frame}


\begin {frame}{Beautify the package}
\begin {itemize}
\item
Fix the indentation.
\codeline {./pre-inst-env guix style python-numpy-illustrated}
\item
Check for real problems.
\codeline {./pre-inst-env guix lint python-numpy-illustrated}
\end {itemize}
\end {frame}


\begin {frame}[fragile]{Prepare git send-email}
\begin {itemize}
\item
Install the \texttt {send-email} output of \texttt {git}.
\codeline {guix install git:send-email}
\item
Edit \texttt {.git/config} or \texttt {\$HOME/.gitconfig}.
\begin {verbatim}
[user]
        email = x.y@aquilenet.fr
        name = Andreas Enge
[sendemail]
        smtpserver = smtp.aquilenet.fr
        smtpuser = x.y@aquilenet.fr
        smtpencryption = tls
        smtpserverport = 587
        smtppass = secret
\end{verbatim}
\end {itemize}
\end{frame}


\begin {frame}{Send the patch and interact with comments}
\begin {itemize}
\item
Send the thing.
\codeline {git send-email --base=origin/master --annotate -1}
\item
Wait for reactions, or check at
\url {https://qa.guix.gnu.org/}.
\item
Reply to mails, or send a new version etc. to
\texttt {xyztu@debbugs.gnu.org}
\end {itemize}
\end {frame}

\end {document}

